const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const bodyParser = require('body-parser');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/user');
const dbService = require('./services/db.service');
const app = express();

const cors = require('cors');
dbService.initDB();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors({origin: '*'}));
app.use('/', indexRouter);
app.use('/user', usersRouter);



module.exports = app;
