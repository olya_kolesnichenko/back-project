function isValidNumber(num) {
    return !isNaN(parseFloat(num)) && isFinite(num) && num>0;
}


module.exports = {
    isValidNumber
};