const express = require('express');
const router = express.Router();

const {getUsers, getUser, updateUser, deleteUser, createUser} = require('../services/user.service');
const {isValidNumber} = require('../helper');

router.post('/', function (req, res, next) {
    const result = req.body;

   if (isValidNumber(result.health) && isValidNumber(result.attack) && (result.defense)){
       result.health = +result.health;
       result.attack = +result.attack;
       result.defense = +result.defense;
       createUser(result).then(function (data) {
           res.send(data)
       }, function (err) {
           res.status(400).send('error' + err);
       });
   }else{
       res.status(400).send('not valid params');
   }

});

router.get('/', function (req, res) {
    getUsers().then(function (result) {
        res.send(result)
    }, function (err) {
        res.status(500);
        res.send(err);
    });

});

router.get('/:id/', function (req, res) {
    getUser(req.params.id).then(function (result) {
        res.send(result)
    }, function (err) {
        res.status(400);
        res.send(err);
    });

});

router.put('/:id/', function (req, res) {

    const result = req.body;


    if (isValidNumber(result.health) && isValidNumber(result.attack) && (result.defense)){
        result.health = +result.health;
        result.attack = +result.attack;
        result.defense = +result.defense;

        updateUser(result, req.params.id).then(function (data) {//get body for req

            res.send(data)
        }, function (err) {
            res.status(400);
            res.send(err);
        });
    }else{
        res.status(400).send('not valid params');
    }


});

router.delete('/:id/', function (req, res) {

    deleteUser(req.params.id).then(function (result) {
        res.status(200);
        res.send({success: true});
    }, function (err) {
        res.status(400);
        res.send(err);
    });

});

module.exports = router;
