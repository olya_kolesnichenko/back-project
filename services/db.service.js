const fs = require('fs');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');


const initDB = () => {
    db.serialize(function () {
        db.run("CREATE TABLE fighters (name TEXT, health INTEGER, attack INTEGER, defense INTEGER, source TEXT)");

        const fighters = JSON.parse(fs.readFileSync("./resources/fighters.json", "utf8"));

        const stmt = db.prepare("INSERT INTO fighters VALUES (?, ?, ?, ?, ?)");
        fighters.forEach(function (item) {
            stmt.run(item.name, item.health, item.attack, item.defense, item.source);
        });

        stmt.finalize();
    });
};

const getDB = () => db;

module.exports = {
    initDB, getDB
};