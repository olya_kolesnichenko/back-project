const { getUsersData, getUserData, updateUserData, deleteUserData, createUserData } = require('../repositories/user.repository');


const getUsers = () => getUsersData();

 const getUser = (id) => {
    return new Promise( function(resolve, reject) {
        if (id>0){
            getUserData(id).then(function (result) {
                resolve(result);
            }, function (err) {
                reject(err)
            })
        }else{
            reject("can't find user")
        }

    });
};

const updateUser = (user, id) => {

    return new Promise( function(resolve, reject) {
        if (user.name){
            updateUserData(user, id).then(function (result) {
                resolve(result);

            }, function (err) {
                reject(err)
            })
        }else{
            reject("cant'update")
        }

    });
};

const deleteUser = (id) => {
    return new Promise( function(resolve, reject) {
        if (id>0){
            deleteUserData(id).then(function (result) {
                resolve(result);
            }, function (err) {
                reject(err)
            })
        }else{
            reject("can't delete")
        }

    });
};

const createUser = (user) => {
    return new Promise( function(resolve, reject) {
        if (user){
            createUserData(user).then(function (result) {
                resolve(result);
            }, function (err) {
                reject(err)
            })
        }else{
            reject("no data to create user")
        }

    });
};


module.exports = {
    getUsers, getUser, updateUser, deleteUser, createUser
};