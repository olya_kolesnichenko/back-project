

## Instalation

`npm install`

`npm run start`

open http://localhost:3000/


GET: /user
get all fighters

GET: /user/:id
get fighter by ID

POST: /user
create fighter
API expects such data: name, health, attack, defense, source(name, source - string, health, attack, defense - number)

PUT: /user/:id
update fighter data by ID
API expects such data: name, health, attack, defense, source(name, source - string, health, attack, defense - number)

DELETE: /user/:id
delete fighter by ID
