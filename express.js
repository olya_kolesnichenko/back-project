const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');
const app = express();
const bodyParser = require("body-parser");
const db = new sqlite3.Database(':memory:');

app.use(bodyParser.urlencoded({ extended: false }));

db.serialize(function() {
    db.run("CREATE TABLE fighters (name TEXT, health INTEGER, attack INTEGER, defense INTEGER, source TEXT)");

    const fighters =   JSON.parse(fs.readFileSync("./resources/fighters.json", "utf8"));

    const stmt = db.prepare("INSERT INTO fighters VALUES (?, ?, ?, ?, ?)");
     fighters.forEach(function(item) {
        stmt.run(item.name, item.health, item.attack, item.defense, item.source);
    });

    stmt.finalize();
});

app.get('/user', function (req, res) {
    db.all("SELECT rowid AS _id, name, health, attack, defense, source FROM fighters", function(err, rows) {
        res.send(rows);
        res.status(200);
    });
});

app.get('/user/:id/', function (req, res) {
    //console.log(req);
    db.all(`SELECT rowid AS _id, name, health, attack, defense, source FROM fighters WHERE _id = ${req.params.id}`, function(err, rows) {
        if (rows.length > 0){
            res.send(rows[0]);
            res.status(200);
        } else {
            res.status(404);
            res.send(`user with id = ${req.params.id} not found`);
        }

    });
});
app.put('/user/:id/', function (req, res) {
    //console.log(req);
    db.all(`UPDATE rowid AS name, health, attack, defense, source FROM fighters WHERE _id = ${req.params.id}`, function(err, rows) {
        if (rows.length > 0){
            res.send(rows[0]);
            res.status(200);
        } else {
            res.status(404);
            res.send(`user with id = ${req.params.id} not found`);
        }

    });
});
app.delete('/user/:id/', function (req, res) {
    //console.log(req);
    db.all(`SELECT rowid AS _id, name, health, attack, defense, source FROM fighters WHERE _id = ${req.params.id}`, function(err, rows) {
        if (rows.length > 0){
            res.send(rows[0]);
            res.status(200);
        } else {
            res.status(404);
            res.send(`user with id = ${req.params.id} not found`);
        }

    });
});

app.post('/user', function (req, res) {
    console.log(req);//check all data
    console.log(req.body);
    res.send(req.body);
    res.status(200);
});

app.get('/*', function (req, res) {
    res.status(404);
    res.send('not found');
});

app.listen(3000);




//
// db.close();