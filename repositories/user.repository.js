const { getDB } = require('../services/db.service');

const getUsersData = () => {
    return new Promise(function (resolve, reject) {
        getDB().all("SELECT rowid AS _id, name, health, attack, defense, source FROM fighters", function (err, rows) {
            resolve(rows);
        });
    });
};

const getUserData = (id) => {
    return new Promise(function (resolve, reject) {
        getDB().all(`SELECT rowid AS _id, name, health, attack, defense, source FROM fighters WHERE _id = ${id}`, function (err, rows) {
            if (rows.length > 0) {
                resolve(rows[0]);
            } else {
                reject(`user with id = ${id} not found`);
            }
        });
    });
};

const updateUserData = (user, id) => {
    return new Promise(function (resolve, reject) {
        getDB().run(`UPDATE fighters SET name=?, health=?, attack=?, defense=?, source=?  WHERE rowid =?`, [user.name, user.health, user.attack, user.defense, user.source, id], function (err, res) {
            if (!err) {
                resolve(true);
            } else {
                reject(`user with id = ${id} not found`);
            }
        });
    });
};

const deleteUserData = (id) => {
    return new Promise(function (resolve, reject) {
        getDB().run(`DELETE FROM fighters WHERE rowid = ${id}`, function (err, res) {
            if (!err) {
                resolve(true);
            } else {
                reject(`user with id = ${id} not found`);
            }

        });
    });
};

const createUserData = (user) => {
    return new Promise(function (resolve, reject) {
        getDB().run(`INSERT INTO fighters VALUES( ?,?,?,?,?)`, [user.name, user.health, user.attack, user.defense, user.source], function (err, rows) {
            if (err) {
                reject(err, "can't create");
            } else {
                resolve(true);
            }
        });
    });
};

module.exports = {
    getUsersData, getUserData, updateUserData, deleteUserData, createUserData
};