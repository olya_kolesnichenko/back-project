const http = require('http');
const fs = require('fs');
const port = 3000;

const logFile = fs.createWriteStream('log.txt', {flags: 'a'});

const requestHandler = (request, response) => {
    console.log(request.url);
    logFile.write(`request to: ${request.url}\n`);
    response.end('Ping-pong');
};

const server = http.createServer(requestHandler);

server.listen(port, (err) =>{
    if(err){
        return console.log('error', err);
    }
    console.log(`server works on http://localhost:${port}`);
});